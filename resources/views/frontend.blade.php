<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{--<meta content="{{route('view.home')}}" name="index_url" />--}}
        <meta content="{{secure_url('/')}}" name="index_url" />
        <meta content="{{csrf_token()}}" name="csrf-token" />
        <title>愛他。愛家。愛爸爸</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">

        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Styles -->
        <link href="{{ secure_asset('css/app.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
        <link rel="stylesheet" href="{{ secure_asset('css/aos.css') }}" />
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
        </style>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-XXXXXX');</script>
        <!-- End Google Tag Manager -->
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/zh_HK/sdk.js#xfbml=1&version=v5.0&appId=147088925695090&autoLogAppEvents=1"></script>
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-XXXXXX"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="overlay-advent-calendar-slider" class="mobile-visible"></div>
        <div id="navmenu"></div>
        @yield("content")
        <script src="{{ secure_asset('js/app.js') }}"></script>
        <!-- AOS Library -->
        <script src="{{ secure_asset('js/aos.js') }}"></script>
        <script>
            AOS.init();
            //Emit GTM
            function emitGTM(action, label){
                let data = {
                    "campaign-name": "bioderma",
                    "event-action": action,
                    "action-label": label
                };
                dataLayer.push(Object.assign({
                    event:"behavior-event",
                }, data));
            }

        </script>
        <a href="/#0" class="cd-top"><img src="{{secure_asset('images/icon-backtotop.png', false)}}" /></a>
        <script>
            $(document).ready(function($){
                var offset = 600,
                    floating_start = 200,
                    floating_end = 660,
                    $floating = $('.floating');

                scroll_top_duration = 200,
                    $back_to_top = $('.cd-top');

                $(window).scroll(function(){
                    ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
                    ( $(this).scrollTop() > floating_start && $(this).scrollTop() < floating_end ) ? $floating.addClass('cd-is-visible') : $floating.removeClass('cd-is-visible cd-fade-out');

                });
                $back_to_top.on('click', function(event){
                    event.preventDefault();
                    $('body,html').animate({
                            scrollTop: 0 ,
                        }, scroll_top_duration
                    );
                });
            });
        </script>
    </body>
</html>

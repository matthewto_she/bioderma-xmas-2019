@extends('frontend')

@section('content')
    @include('partials.header')
    {{--<section class="kv">--}}
        {{--<div class="max-width">--}}
            {{--<img class="lovedad" src="{{asset('images/love-dad.png', false)}}" />--}}
        {{--</div>--}}
            {{--<img class="" src="{{asset('images/kv.jpg', false)}}" />--}}
    {{--</section>--}}
    <section id="kv" class="scroll">
        <div class="padding-wrapper">
            <div class="content">

            </div>
        </div>
    </section>

    <section id="advent-calendar-section" data-aos="fade-up">
        <div id="doctorsay-title" class="title home-title double-row" data-aos="fade-up">
            BIODERMA<br class="mobile-visible" />
            首個聖誕倒數月曆<br />
            <span>隆重登場！</span>
        </div>
        <div id="advent-calendar-slider-desktop">
        </div>
    </section>
    <section id="product-container1" class="scroll" data-aos="fade-up">
        <div class="padding-wrapper">
            <div class="main-title theme-color">
                <div class="background title">
                    皇牌卸妝禮盒
                </div>
            </div>
            <div class="content">
                <div class="products-container desktop-visible">
                    <div class="product">
                        <div class="img-wrapper">
                            <img src="{{ asset('/images/product1.png') }}" />
                        </div>
                        <div class="description">
                            SENSIBIO<br />
                            醫學深層卸妝禮盒<br />
                            <br />
                            (參考價 HK$463)<br />
                            <br />
                            <br />
                        </div>
                    </div>
                    <div class="product">
                        <div class="img-wrapper">
                            <img src="{{ asset('/images/product2.png') }}" />
                        </div>
                        <div class="description">
                            HYDRABIO<br />
                            醫學保濕卸妝禮盒<br />
                            <br />
                            (參考價 HK$463)<br />
                            <br />
                            BIODERMA概念店及屈臣氏有售
                        </div>
                    </div>
                    <div class="product">
                        <div class="img-wrapper">
                            <img src="{{ asset('/images/product3.png') }}" />
                        </div>
                        <div class="description">
                            SENSIBIO<br />
                            醫學深層卸淨體驗禮盒<br />
                            <br />
                            (參考價 HK$264)<br />
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
                <div id="product-slider1" class="mobile-visible"></div>
            </div>
        </div>
        <div class="separator">
            <img src="{{asset('images/separator.png')}}" />
        </div>
    </section>
    <section id="product-container2" class="scroll" data-aos="fade-up">
        <div class="padding-wrapper">
            <div class="main-title theme-color">
                <div class="background title">
                    保濕滋潤精選
                </div>
            </div>
            <div class="content">
                <div class="products-container desktop-visible">
                    <div class="product">
                        <div class="img-wrapper">
                            <img src="{{ asset('/images/product4.png') }}" />
                        </div>
                        <div class="description">
                            HYDRABIO<br />
                            醫學3步循環保濕禮盒<br />
                            <br />
                            (參考價 HK$617)<br />
                            <br />
                            <br />
                        </div>
                    </div>
                    <div class="product">
                        <div class="img-wrapper">
                            <img src="{{ asset('/images/product5.png') }}" />
                        </div>
                        <div class="description">
                            MATRICIUM<br />
                            再生水體驗裝<br />
                            <br />
                            (參考價 HK$246)<br />
                            <br />
                            BIODERMA概念店及屈臣氏有售
                        </div>
                    </div>
                    <div class="product">
                        <div class="img-wrapper">
                            <img src="{{ asset('/images/product6.png') }}" />
                        </div>
                        <div class="description">
                            SENSIBIO<br />
                            醫學深層卸淨體驗禮盒<br />
                            <br />
                            (參考價 HK$264)<br />
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
                <div id="product-slider2" class="mobile-visible"></div>
            </div>
        </div>
        <div class="separator">
            <img src="{{asset('images/separator2.png')}}" />
        </div>
    </section>
    <section id="product-container3" class="scroll" data-aos="fade-up">
        <div class="padding-wrapper">
            <div class="main-title theme-color">
                <div class="background title">
                    保濕滋潤精選
                </div>
            </div>
            <div class="content">
                <div class="products-container desktop-visible">
                    <div class="product">
                        <div class="img-wrapper">
                            <img src="{{ asset('/images/product7.png') }}" />
                        </div>
                        <div class="description">
                            榮獲總評最高的潤手霜<br/>
                            ATODERM柔潤修護套裝<br/>
                            <br/>
                            (參考價 HK$255)<br/>
                            <br/>
                            BIODERMA概念店限定
                        </div>
                    </div>
                </div>
                <div id="product-slider3" class="mobile-visible"></div>
            </div>
        </div>
        <div class="separator">
            <img src="{{asset('images/separator3.png')}}" />
        </div>
    </section>
    <section id="footer">
        <div class="padding-wrapper">
            <div>
                <a href="https://www.facebook.com/Bioderma.HongKong/" target="_blank">
                    <img src="{{asset('images/fb-like.png')}}" />
                </a>
                {{--<div class="fb-like" data-href="https://www.facebook.com/Bioderma.HongKong/" data-width="" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>--}}
                <a href="https://www.facebook.com/Bioderma.HongKong/" target="_blank">
                    <img src="{{asset('images/fb-page.png')}}" />
                </a>
            </div>
            <div>
                ©2019 BIODERMA版權所有
            </div>
        </div>
    </section>

    {{--<script>--}}
        {{--function articleGTM(id) {--}}
            {{--let data = {--}}
                {{--'campaign-name': 'sanofi',--}}
                {{--'event-action': 'Click',--}}
                {{--'action-label': 'interview-written' + id--}}
            {{--};--}}
            {{--dataLayer.push(Object.assign({--}}
                {{--event: 'behavior-event',--}}
            {{--}, data));--}}
        {{--}--}}
    {{--</script>--}}
@endsection
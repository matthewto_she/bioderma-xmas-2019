export const advent_calendar_data = [
    {
        image: 'doctor-video-slider/doctor1.jpg',
        name: '施俊健醫生<br />陳樹賢醫生',
        description: '1近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor2.jpg',
        name: '林楚文醫生<br />張源津醫生',
        description: '2近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor3.jpg',
        name: '潘明駿醫生<br />潘明駿醫生',
        description: '3近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor4.jpg',
        name: '梁耀霖醫生<br />梁廣泉醫生',
        description: '4近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor1.jpg',
        name: '施俊健醫生<br />陳樹賢醫生',
        description: '5近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor2.jpg',
        name: '林楚文醫生<br />張源津醫生',
        description: '6近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor3.jpg',
        name: '潘明駿醫生<br />潘明駿醫生',
        description: '7近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor4.jpg',
        name: '梁耀霖醫生<br />梁廣泉醫生',
        description: '8近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor1.jpg',
        name: '施俊健醫生<br />陳樹賢醫生',
        description: '9近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor2.jpg',
        name: '林楚文醫生<br />張源津醫生',
        description: '10近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor3.jpg',
        name: '潘明駿醫生<br />潘明駿醫生',
        description: '11近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
    {
        image: 'doctor-video-slider/doctor4.jpg',
        name: '梁耀霖醫生<br />梁廣泉醫生',
        description: '12近年前列腺癌和大腸癌都成為男士癌病殺手，到底要怎樣才可以預防？兩種癌病又有甚麼病徵？今次我們誠邀16位醫生為我們一一拆解！',
    },
];
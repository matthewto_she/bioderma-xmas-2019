export const slider_data = {
    'map-prostate-early': [
        {
            image: 'p-early1.png',
            title: '外科手術',
            description: '以手術切除前列腺及附近的健康組織，適合早期仍未擴散的局部腫瘤。手術後或有可能出現尿失禁或勃起功能障礙<sup>10,11</sup>'
        },
        {
            image: 'p-early2.png',
            title: '放射性治療',
            description: '利用體外高能量X光射線或放置放射物質於體內腫瘤附近，以消滅癌細胞；可作為手術後治療或配合雄激素剝奪治療進行。然而，治療或有可能引致泌尿系統、腸胃或性功能的副作用<sup>10,11</sup>。'
        },
        {
            image: 'p-early3.png',
            title: '雄激素剝奪治療(ADT)',
            description: '透過壓抑體內雄激素的水平來控制腫瘤的生長。可採用手術切除睪丸或藥物治療，如促黃體素釋放激素致效劑/拮抗劑(LHRH agonists / antagonists)，或抗雄激素(anti-androgens)。ADT治療或有可能導致潮熱、性功能障礙或引致骨質疏鬆<sup>10-12</sup>。'
        },
    ],
    'map-prostate-late': [
        {
            image: 'p-late1.png',
            title: '化療',
            description: '屬全身性治療，沿循環系統運行殺死癌細胞，以控制腫瘤生長及擴散。對仍受雄激素影響及已出現抗性的患者均有效。注意或有可能出現化療的相關副作用，例如脫髮、嘔吐、疲倦等<sup>10,11,13</sup>。'
        },
        {
            image: 'p-late2.png',
            title: '雄激素剝奪治療 (ADT)',
            description: '透過以手術切除睪丸或以藥物抑制雄激素的水平，藉此控制腫瘤生長，紓緩徵狀。對於仍會對雄激素出現抗性的患者，應持續以雄激素剝奪治療作為第一線治療<sup>10,11</sup>。'
        },
        {
            image: 'p-late3.png',
            title: '放射性同位素 (鐳-223)',
            description: '以α射線物質透過靜脈注射進體內，針對骨質密度低的位置，減低骨痛及因骨骼擴散導致骨折的風險；適合癌細胞已擴散到骨骼的患者。或有機會因血球數量下降引致貧血，容易出血或易受感染<sup>10,11,14</sup>。'
        },
        {
            image: 'p-late4.png',
            title:'免疫療法',
            description: '抽取自體免疫細胞，經處理後透過靜脈輸注放回體內，藉以刺激免疫系統消滅癌細胞。適合較少或無出現徵狀，並對雄激素剝奪出現抗性，而從未接受過化療的患者。而大多數患者出現的副作用與靜脈輸注反應有關<sup>10,15</sup><br /><span>* 此治療並未於香港註冊使用</span>'
        },
        {
            image: 'p-late5.png',
            title: '荷爾蒙標靶治療',
            description: '抑制睪丸及腎上腺分泌雄激素的功能，藉以阻止腫瘤生長。適合雄激素剝奪出現抗性或仍受雄激素影響的患者。治療期間或會出現高血壓、低血鉀或水腫的副作用<sup>10,11,13</sup>。'
        }
    ],
    'map-colorectal-early': [
        {
            image: 'p-early1.png',
            title: '外科手術',
            description: '以手術切除有癌細胞生長的部份腸道及淋巴結；適合早期仍未擴散的腫瘤<sup>11</sup>。'
        },
        {
            image: 'p-late1.png',
            title: '化療',
            description: '屬全身性治療，沿循環系統運行殺死癌細胞，從而控制腫瘤生長及縮小腫瘤。如手術後適當使用化療，可減低復發風險<sup>11,12</sup>。'
        },
        {
            image: 'p-late3.png',
            title: '放射性治療',
            description: '利用高能量X光射線局部消滅癌細胞；適合無法以手術切除的腫瘤<sup>11,12</sup>。'
        },
    ],
    'map-colorectal-late': [
        {
            image: 'p-late5.png',
            title: '標靶治療',
            description: '抗血管增生藥物阻止連接到腫瘤的血管增生，從而堵截血液供應，以控制腫瘤生長<sup>11,13</sup>。<br /><br />表皮細胞生長因子抑制劑(anti-EGFR)：抑制腫瘤的生長速度<sup>12,13</sup>。'
        },
        {
            image: 'p-late1.png',
            title: '化療',
            description: '屬全身性治療，綜合多種化學治療藥物，沿循環系統運行消滅癌細胞；目的是控制腫瘤生長，可作紓緩性治療<sup>11,13</sup>。'
        },
        {
            image: 'c-late3.png',
            title: '免疫治療',
            description: '利用針對特定機制的單株抗體，加強身體免疫反應以消滅癌細胞<sup>11,13</sup>。'
        },
    ],

}


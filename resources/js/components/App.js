// resources/assets/js/components/App.js

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import Overlay from './Overlay'
import OverlayAdventCalendarSlider from './OverlayAdventCalendarSlider'
import {baseURL} from "../common";
import Slider from "react-slick";

class App extends Component {
    constructor(props){
        super()
        this.state = {
            showOverlay: false
        }

        this.el = document.createElement('div');
        // this.onHide = this.onHide.bind(this)
    }

    render () {


    }
}

// ReactDOM.render(<App />, document.getElementById('app'))

function NextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            // style={{ ...style, display: "block", background: "red" }}
            onClick={onClick}
        >
            <img src={'images/arrow-next-white.svg'} />
            {/*<img className={'desktop-visible'} src={'images/arrow-next-black.svg'} />*/}
        </div>
    );
}

function PrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            // style={{ ...style, display: "block", background: "green" }}
            onClick={onClick}
        >
            <img src={'images/arrow-prev-white.svg'} />
            {/*<img className={'desktop-visible'} src={'images/arrow-prev-black.svg'} />*/}
        </div>
    );
}

const settings1 = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    touchThreshold:10,
    nextArrow: <NextArrow/>,
    prevArrow: <PrevArrow/>,
};

if(document.getElementById('product-slider1')){
    ReactDOM.render(
        <Slider {...settings1}>
            <div className="product">
                <div className="img-wrapper">
                    <img src={baseURL+'/images/product1.png'}/>
                </div>
                <div className="description">
                    SENSIBIO<br/>
                    醫學深層卸妝禮盒<br/>
                    <br/>
                    (參考價 HK$463)<br/>
                    <br/>
                    <br/>
                </div>
            </div>
            <div className="product">
                <div className="img-wrapper">
                    <img src={baseURL+'/images/product2.png'}/>
                </div>
                <div className="description">
                    HYDRABIO<br/>
                    醫學保濕卸妝禮盒<br/>
                    <br/>
                    (參考價 HK$463)<br/>
                    <br/>
                    BIODERMA概念店及屈臣氏有售
                </div>
            </div>
            <div className="product">
                <div className="img-wrapper">
                    <img src={baseURL+'/images/product3.png'}/>
                </div>
                <div className="description">
                    SENSIBIO<br/>
                    醫學深層卸淨體驗禮盒<br/>
                    <br/>
                    (參考價 HK$264)<br/>
                    <br/>
                    <br/>
                </div>
            </div>
        </Slider>,
        document.getElementById('product-slider1')
    )
}
if(document.getElementById('product-slider2')){
    ReactDOM.render(
        <Slider {...settings1}>
            <div className="product">
                <div className="img-wrapper">
                    <img src={baseURL+'/images/product4.png'}/>
                </div>
                <div className="description">
                    HYDRABIO<br/>
                    醫學3步循環保濕禮盒<br/>
                    <br/>
                    (參考價 HK$617)<br/>
                    <br/>
                    <br/>
                </div>
            </div>
            <div className="product">
                <div className="img-wrapper">
                    <img src={baseURL+'/images/product5.png'}/>
                </div>
                <div className="description">
                    MATRICIUM<br/>
                    再生水體驗裝<br/>
                    <br/>
                    (參考價 HK$246)<br/>
                    <br/>
                    BIODERMA概念店及屈臣氏有售
                </div>
            </div>
            <div className="product">
                <div className="img-wrapper">
                    <img src={baseURL+'/images/product6.png'}/>
                </div>
                <div className="description">
                    SENSIBIO<br/>
                    醫學深層卸淨體驗禮盒<br/>
                    <br/>
                    (參考價 HK$264)<br/>
                    <br/>
                    <br/>
                </div>
            </div>
        </Slider>,
        document.getElementById('product-slider2')
    )
}

if(document.getElementById('product-slider3')){
    ReactDOM.render(
        <Slider {...settings1}>
            <div className="product">
                <div className="img-wrapper">
                    <img src={baseURL+'/images/product7.png'}/>
                </div>
                <div className="description">
                    榮獲總評最高的潤手霜<br/>
                    ATODERM柔潤修護套裝<br/>
                    <br/>
                    (參考價 HK$255)<br/>
                    <br/>
                    BIODERMA概念店限定
                </div>
            </div>
        </Slider>,
        document.getElementById('product-slider3')
    )
}

if(document.getElementById('navbar')) {
    ReactDOM.render(<Header />, document.getElementById('navbar'))
}
if(document.getElementById('overlay-advent-calendar-slider')) {
    ReactDOM.render(<OverlayAdventCalendarSlider />, document.getElementById('overlay-advent-calendar-slider'))
}
// resources/assets/js/components/Header.js

import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import {FontIcon} from "react-md";
import ReactDOM from "react-dom";
import {baseURL} from "../common";


// const Header = () => (
//     <nav className='navbar navbar-expand-md navbar-light navbar-laravel'>
//         <div className='container'>
//             <Link className='navbar-brand' to='/'>Tasksman</Link>
//         </div>
//     </nav>
// )
//
// export default Header

class Header extends Component{
    constructor(props){
        super(props)
        this.state = {
            showMenu:false,
            fixed:true,
            scrollTopPosition: 0
        }

        this.toggleMenu = this.toggleMenu.bind(this)
        this.handleScroll = this.handleScroll.bind(this)
        this.isScrolledIntoView = this.isScrolledIntoView.bind(this)
    }

    toggleMenu(){
        this.setState({
            showMenu: !this.state.showMenu
        })
    }

    handleScroll(e){
        let element = e.target

        let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        // if(this.isScrolledIntoView(document.getElementById('navbar'),scrollTop)){
            // console.log(document.documentElement.scrollTop)
        // }

        let direction = (scrollTop > this.state.scrollTopPosition && scrollTop > 0) ? true : false;
        this.setState({
            scrollTopPosition:document.documentElement.scrollTop,
            fixed: !direction
        });

        // console.log('element.scrollHeight',element.scrollHeight)
        // console.log('element.scrollTop',element.scrollTop)
        // console.log('element.clientHeight',element.clientHeight)
    }

    isScrolledIntoView(elm,scrolltop){

        let docViewTop = scrolltop;
        let docViewBottom = docViewTop + ( window.outerHeight==0 ? window.innerHeight : window.outerHeight );

        let elemTop = elm.getBoundingClientRect().top + docViewTop;
        let elemBottom = elemTop + elm.getBoundingClientRect().height;

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    componentWillUnmount(){
        window.removeEventListener('scroll', this.handleScroll);
    }

    componentDidMount(){
        window.addEventListener('scroll', this.handleScroll);
    }

    render(){
        // const { onClose } = this.props
        const { showMenu } = this.state
        if(document.getElementById('navmenu')) {
            ReactDOM.render(
            <div id="navmenu-content" className={showMenu ? 'show' : 'hide'}>
                <div className="menu-close" onClick={()=>{
                    this.toggleMenu()
                }}>
                    <FontIcon>close</FontIcon>
                </div>
                <br />
                <br />
                <h2><a href={baseURL} onClick={()=>{
                    this.toggleMenu()
                }}>主頁</a></h2>
                <h2><a href={baseURL+"/prostate"} onClick={()=>{
                    this.toggleMenu()
                }}>關於前列腺癌</a></h2>
                <h2><a href={baseURL+"/colorectal"}  onClick={()=>{
                    this.toggleMenu()
                }}>關於大腸癌</a></h2>
                <h2><a href={baseURL+"#upload-title"} onClick={()=>{
                    this.toggleMenu()
                }}>全城‧傳愛</a></h2>
            </div>, document.getElementById('navmenu'))
        }
        const homeIcon = (window.location.href.indexOf('prostate') > 0) ? <a href={baseURL+"/"}>
            <img className="home-icon" src={baseURL+"/images/home-prostate.svg"}/>
        </a>: (window.location.href.indexOf('colorectal') > 0) ? <a href={baseURL+"/"}>
            <img className="home-icon" src={baseURL+"/images/home-colorectal.svg"}/>
        </a>: '';

        const items = (window.location.href.indexOf('prostate') == -1 && window.location.href.indexOf('colorectal') == -1) ?
            <div>
                <a href={baseURL+"/"}>
                    <img className="header-logo" src={baseURL+"/images/header-logo.png"}/>
                </a>
                {/*<a href={baseURL}>主頁</a>*/}
                {/*<a href={baseURL+"/prostate"}>關於前列腺癌</a>*/}
                {/*<a href={baseURL+"/colorectal"}>關於大腸癌</a>*/}
                {/*<a href={(window.location.href.indexOf('article') == -1) ? "#upload-title" : baseURL+"#upload-title"}>全城‧傳愛</a>*/}
            </div>: <a href={baseURL+"/"}>
                <img className="header-logo" src={baseURL+"/images/header-logo.png"}/>
            </a>;

        const navIcon = (window.location.href.indexOf('prostate') == -1 && window.location.href.indexOf('colorectal') == -1) ?
            <div className="mobile-visible" onClick={() => {
                this.toggleMenu()
            }}>
                <img className="nav-icon" src={baseURL+"/images/nav.svg"}/>
            </div>:'';

        return (
            <section className={(this.state.fixed) ? 'fixed header padding-wrapper' : 'header padding-wrapper'}>
                <div className="max-width">
                    {homeIcon}
                    <div className="desktop-visible">
                        {items}
                    </div>
                    <div className="mobile-visible">
                        <a href={baseURL+"/"}>
                            <img className="header-logo" src={baseURL+"/images/header-logo.png"} />
                        </a>
                    </div>
                    {navIcon}
                </div>
            </section>
        );
    }
}

export default Header;

import React, { Component } from "react";
import Slider from "react-slick";
import { FontIcon } from 'react-md'
// import {slider_data} from "../data/slider";
import {advent_calendar_data} from "../data/advent_calendar";

export default class AdventCalendarSlider extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            slideIndex: this.props.slickGoTo - 1
        }
        this.changeIndex = this.changeIndex.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        if(this.props != nextProps) {
            this.setState({
                slideIndex: nextProps.slickGoTo - 1
            },function(){
                this.slider.slickGoTo(this.state.slideIndex, true)
            });

        }
    }

    changeIndex(idx){
        console.log(typeof this.props.changeIndex)
        if(typeof this.props.changeIndex != 'undefined'){
            const {changeIndex} = this.props.changeIndex
            changeIndex(idx)
        }
    }

    render() {
        const NextArrow = function(props) {
            const { className, style, onClick } = props;
            return (
                <div
                    className={className}
                    onClick={onClick}
                >
                    <FontIcon>chevron-right</FontIcon>
                </div>
            );
        }

        const PrevArrow = function(props) {
            const { className, style, onClick } = props;
            return (
                <div
                    className={className}
                    onClick={onClick}
                >
                    <FontIcon>chevron-left</FontIcon>
                </div>
            );
        }

        const that = this

        // const settings = {
        //     dots: false,
        //     lazyLoad: false,
        //     infinite: false,
        //     speed: 500,
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //     initialSlide: this.state.slideIndex,
        //     // nextArrow:<NextArrow/>,
        //     // prevArrow:<PrevArrow/>,
        //     responsive:[
        //         {
        //             breakpoint:690,
        //             settings:{
        //                 centerPadding:'0px',
        //                 slidesToShow:1,
        //                 infinite: false,
        //                 dots: false,
        //             }
        //         }
        //     ],
        //     beforeChange:(current, next) => {
        //         that.changeIndex(next)
        //     }
        // };

        return (
            <Slider
                ref={slider => (this.slider = slider)}
                dots={false}
                className={'center'}
                centerMode={false}
                infinite={true}
                centerPadding={'0px'}
                slidesToShow={1}
                speed={500}
                rows={1}
                slidesPerRow={1}
                // nextArrow={<NextArrow />}
                // prevArrow={<PrevArrow />}
                responsove={[
                    {
                        breakpoint:690,
                        settings:{
                            centerPadding:'0px',
                            slidesToShow:3,
                            centerMode: true,
                            rows: 1,
                        }
                    }
                ]}
                beforeChange={
                    (current, next) => {
                        that.changeIndex(next)
                    }
                }
            >
                {
                    advent_calendar_data.map((item,idx)=>{
                        return <div className="tip" key={idx}>
                            <p dangerouslySetInnerHTML={{ __html:item.description }}></p>
                       </div>
                    })
                }
            </Slider>
        );
    }
}
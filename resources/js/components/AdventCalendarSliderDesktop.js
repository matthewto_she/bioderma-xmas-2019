import React, { Component } from "react";
import Slider from "react-slick";
import { FontIcon } from 'react-md'
import {baseURL, isMobile} from "../common";
import {advent_calendar_data} from "../data/advent_calendar";
import axios from 'axios';

function NextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            // style={{ ...style, display: "block", background: "red" }}
            onClick={onClick}
        >
            <img src={'images/arrow-next-white.svg'} />
            {/*<img className={'mobile-visible'} src={'images/arrow-next-white.svg'} />*/}
            {/*<img className={'desktop-visible'} src={'images/arrow-next-black.svg'} />*/}
        </div>
    );
}

function PrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            // style={{ ...style, display: "block", background: "green" }}
            onClick={onClick}
        >
            <img src={'images/arrow-prev-white.svg'} />
            {/*<img className={'mobile-visible'} src={'images/arrow-prev-white.svg'} />*/}
            {/*<img className={'desktop-visible'} src={'images/arrow-prev-black.svg'} />*/}
        </div>
    );
}

export default class AdventCalendarSliderDesktop extends Component {
    constructor(props){
        super(props);
        this.state = {
            showOverlay: false,
            name: 0,
            name1: 0,
            name2: 0,
            days: 99
            // name3: 0,
        }
        this.togglePopup = this.togglePopup.bind(this);
        this.closeOverlay = this.closeOverlay.bind(this);

    }

    componentWillMount(){
        const that = this
        axios.get(`${baseURL}/api/adventcalendar`)
            .then((response) => {
                if(response.status == 200){
                    that.setState({
                        days: response.data
                    }, () => {
                        console.log(that.state.days)
                    });
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }

    componentDidMount() {
        // this.setState({
            // name1: this.slider1,
            // name2: this.slider2,
            // name3: this.slider3
        // });
    }

    togglePopup(name){
        const { toggleOverlay } = this.props
        toggleOverlay(name);
        this.setState({
            name1: name,
            showOverlay: true
        })
        // slickGoTo(name);
    }

    closeOverlay(){
        this.setState({
            showOverlay: false
        })
    }

    render() {
        const {name1, days} = this.state

        return (
            <div className={'two-container'}>
                <div className={'block desktop-visible'}>
                    {this.state.showOverlay ?
                        <div>
                            <Slider
                                // asNavFor={name1}
                                ref={slider => (this.slider1 = slider)}
                                dots={true}
                                className={'main center'}
                                // centerMode={true}
                                infinite={true}
                                initialSlide={this.state.name1}
                                centerPadding={'0px'}
                                slidesToShow={1}
                                speed={500}
                                rows={1}
                                slidesPerRow={1}
                                touchThreshold={10}
                                // nextArrow={<NextArrow />}
                                // prevArrow={<PrevArrow />}
                                responsove={[
                                    {
                                        breakpoint:690,
                                        settings:{
                                            centerPadding:'0px',
                                            slidesToShow:3,
                                            rows: 1,
                                        }
                                    }
                                ]}
                                beforeChange={(current, next) =>
                                    this.setState({ name1: next })
                                }
                            >
                                {
                                    advent_calendar_data.map((item,idx)=>{
                                        return <div data-aos="fade-in">{item.description}</div>
                                    })
                                }
                            </Slider>
                            <div className={"btn-close"} onClick={() => this.closeOverlay()}>
                                close
                            </div>
                        </div>
                    : ''}
                </div>
                <section class="advent-calendar-container" data-aos="fade-up">
                {
                    advent_calendar_data.map((item,idx)=>{
                        const disabled = ( days < -idx ? 'disabled' : '')
                        const selected = name1 == idx ? 'selected' : ''
                        // return disabled ?
                        //     <div className={`box ${disabled} ${selected}`}>
                        //         <img src={'images/'+item.image} />
                        //         {/*<div class="article-title" dangerouslySetInnerHTML={{ __html:item.title }}></div>*/}
                        //     </div> :
                        //     <a href="#doctorsay-title" key={idx} onClick={() => {
                        //         this.togglePopup(idx);
                        //         this.slider1.slickGoTo(idx)
                        //     }}>
                        //         <div className={`box ${disabled} ${selected}`}>
                        //             <img src={'images/' + item.image}/>
                        //             {/*<div className="article-title" dangerouslySetInnerHTML={{__html: item.title}}></div>*/}
                        //         </div>
                        //     </a>
                        return <div
                                key={idx} onClick={() => {
                                this.togglePopup(idx);
                                this.slider1.slickGoTo(idx)
                            }}
                            className={`box ${disabled} ${selected}`}
                            id={`box${idx+1}`}
                        ></div>
                    })
                }
                </section>
            </div>
        );
    }
}
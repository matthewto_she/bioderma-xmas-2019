// resources/assets/js/components/App.js

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import Map from './Map'
import Overlay from './Overlay'
// import CureSlider from './CureSlider'
import ImageMapper from 'react-image-mapper';

class OverlaySlider extends Component {
    constructor(props){
        super(props)
        this.state = {
            showOverlay: false,
            name: 1
        }

        // this.el = document.createElement('div');
        // this.onHide = this.onHide.bind(this)
        this.toggleOverlay = this.toggleOverlay.bind(this);
        this.slickGoTo = this.slickGoTo.bind(this);
    }

    toggleOverlay(){
        this.setState({
            showOverlay: !this.state.showOverlay
        })
    }

    slickGoTo(name){
        this.setState({
            name: name
        })
    }
    render () {

        let mapProstateEarly = {
            name: "map-early",
            areas: [
                { name: "1", label: "prostate-early1", shape: "poly", coords: [1,0,1,98,239,99,204,0]},
                { name: "2", label: "prostate-early2", shape: "poly", coords: [221,3,251,96,445,96,473,2]},
                { name: "3", label: "prostate-early3", shape: "poly", coords: [484,2,458,98,696,97,695,2]},
            ]
        }

        let mapProstateLate = {
            name: "map-late",
            areas: [
                { name: "1", label: "prostate-diffusion1", shape: "poly", coords: [1,3,3,96,160,99,176,4]},
                { name: "2", label: "prostate-diffusion2", shape: "poly", coords: [443,4,461,99,698,100,699,1]},
                { name: "3", label: "prostate-diffusion3", shape: "poly", coords: [191,1,173,98,448,99,430,3]},
                { name: "4", label: "prostate-diffusion4", shape: "poly", coords: [0,108,2,199,315,199,323,109]},
                { name: "5", label: "prostate-diffusion5", shape: "poly", coords: [335,110,328,200,698,199,699,111]},
            ]
        }

        let mobileMapProstateEarly = {
            name: "mobile-map-early",
            areas: [
                { name: "1", label: "prostate-early1", shape: "poly", coords: [0,1,0,124,133,137,112,2]},
                { name: "2", label: "prostate-early2", shape: "poly", coords: [122,1,144,140,277,154,277,2]},
                { name: "3", label: "prostate-early3", shape: "poly", coords: [0,132,0,249,278,251,278,165]},
            ]
        }

        let mobileMapProstateLate = {
            name: "mobile-map-late",
            areas: [
                { name: "1", label: "prostate-diffusion1", shape: "poly", coords: [0,0,0,127,133,135,133,1]},
                { name: "2", label: "prostate-diffusion2", shape: "poly", coords: [146,1,146,170,275,180,275,3]},
                { name: "3", label: "prostate-diffusion3", shape: "poly", coords: [2,138,2,279,131,266,134,150]},
                { name: "4", label: "prostate-diffusion4", shape: "poly", coords: [2,291,0,406,133,405,133,281]},
                { name: "5", label: "prostate-diffusion5", shape: "poly", coords: [144,182,144,404,278,405,278,194]},
            ]
        }

        let mapColorectalEarly = {
            name: "map-early",
            areas: [
                { name: "1", label: "colorectal-early1", shape: "poly", coords: [1,1,2,99,236,98,205,1]},
                { name: "2", label: "colorectal-early2", shape: "poly", coords: [219,4,249,97,431,97,458,3]},
                { name: "3", label: "colorectal-early3", shape: "poly", coords: [467,3,443,96,694,96,694,1]},
            ]
        }

        let mapColorectalLate = {
            name: "map-late",
            areas: [
                { name: "1", label: "colorectal-diffusion1", shape: "poly", coords: [4,3,4,97,216,96,232,4]},
                { name: "2", label: "colorectal-diffusion2", shape: "poly", coords: [442,2,458,97,694,96,695,3]},
                { name: "3", label: "colorectal-diffusion3", shape: "poly", coords: [244,3,228,97,444,96,427,3]},
            ]
        }

        let mobileMapColorectalEarly = {
            name: "mobile-map-early",
            areas: [
                { name: "1", label: "colorectal-early1", shape: "poly", coords: [0,3,0,151,133,141,157,1]},
                { name: "2", label: "colorectal-early2", shape: "poly", coords: [168,4,146,138,275,127,275,2]},
                { name: "3", label: "colorectal-early3", shape: "poly", coords: [3,163,2,246,277,247,277,140]},
            ]
        }

        let mobileMapColorectalLate = {
            name: "mobile-map-late",
            areas: [
                { name: "1", label: "colorectal-diffusion1", shape: "poly", coords: [0,1,2,267,116,268,149,3]},
                { name: "2", label: "colorectal-diffusion2", shape: "poly", coords: [162,0,146,126,278,126,277,2]},
                { name: "3", label: "colorectal-diffusion3", shape: "poly", coords: [144,141,131,266,275,268,275,140]},
            ]
        }

        if( this.props.overlayId == 'overlay-prostate-early' && document.getElementById('map-prostate-early')){
            ReactDOM.render(<Map
                imgUrl={'images/prostate-treatment-early.png'}
                areaMap={mapProstateEarly}
                id={'map-prostate-early'}
                overlay={'overlay-prostate'}
                toggleOverlay={this.toggleOverlay}
                slickGoTo={this.slickGoTo}
            />, document.getElementById('map-prostate-early'))
        }
        if( this.props.overlayId == 'overlay-prostate-late' && document.getElementById('map-prostate-late')){
            ReactDOM.render(<Map
                imgUrl={'images/prostate-treatment-late.png'}
                areaMap={mapProstateLate}
                id={'map-prostate-late'}
                overlay={'overlay-prostate'}
                toggleOverlay={this.toggleOverlay}
                slickGoTo={this.slickGoTo}

            />, document.getElementById('map-prostate-late'))
        }
        if( this.props.overlayId == 'overlay-prostate-early' && document.getElementById('m-map-prostate-early')){
            ReactDOM.render(<Map
                imgUrl={'images/m-prostate-treatment-early.png'}
                areaMap={mobileMapProstateEarly}
                id={'map-prostate-early'}
                overlay={'overlay-prostate'}
                toggleOverlay={this.toggleOverlay}
                slickGoTo={this.slickGoTo}

            />, document.getElementById('m-map-prostate-early'))
        }
        if( this.props.overlayId == 'overlay-prostate-late' && document.getElementById('m-map-prostate-late')){
            ReactDOM.render(<Map
                imgUrl={'images/m-prostate-treatment-late.png'}
                areaMap={mobileMapProstateLate}
                id={'map-prostate-late'}
                overlay={'overlay-prostate'}
                toggleOverlay={this.toggleOverlay}
                slickGoTo={this.slickGoTo}

            />, document.getElementById('m-map-prostate-late'))
        }
        if( this.props.overlayId == 'overlay-colorectal-early' && document.getElementById('map-colorectal-early')){
            ReactDOM.render(<Map
                imgUrl={'images/colorectal-treatment-early.png'}
                areaMap={mapColorectalEarly}
                id={'map-colorectal-early'}
                overlay={'overlay-colorectal'}
                toggleOverlay={this.toggleOverlay}
                slickGoTo={this.slickGoTo}

            />, document.getElementById('map-colorectal-early'))
        }
        if( this.props.overlayId == 'overlay-colorectal-late' && document.getElementById('map-colorectal-late')){
            ReactDOM.render(<Map
                imgUrl={'images/colorectal-treatment-late.png'}
                areaMap={mapColorectalLate}
                id={'map-colorectal-late'}
                overlay={'overlay-colorectal'}
                toggleOverlay={this.toggleOverlay}
                slickGoTo={this.slickGoTo}

            />, document.getElementById('map-colorectal-late'))
        }
        if( this.props.overlayId == 'overlay-colorectal-early' && document.getElementById('m-map-colorectal-early')){
            ReactDOM.render(<Map
                imgUrl={'images/m-colorectal-treatment-early.png'}
                areaMap={mobileMapColorectalEarly}
                id={'map-colorectal-early'}
                overlay={'overlay-colorectal'}
                toggleOverlay={this.toggleOverlay}
                slickGoTo={this.slickGoTo}

            />, document.getElementById('m-map-colorectal-early'))
        }
        if( this.props.overlayId == 'overlay-colorectal-late' && document.getElementById('m-map-colorectal-late')) {
            ReactDOM.render(<Map
                imgUrl={'images/m-colorectal-treatment-late.png'}
                areaMap={mobileMapColorectalLate}
                id={'map-colorectal-late'}
                overlay={'overlay-colorectal'}
                toggleOverlay={this.toggleOverlay}
                slickGoTo={this.slickGoTo}

            />, document.getElementById('m-map-colorectal-late'))
        }

        return (
            <Overlay
                showOverlay={this.state.showOverlay}
                toggleOverlay={this.toggleOverlay}
            >
                <CureSlider id={this.props.id} slickGoTo={this.state.name}></CureSlider>
            </Overlay>
        )

    }
}


export default OverlaySlider;
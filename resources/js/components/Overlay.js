import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FontIcon } from 'react-md'

class Overlay extends Component{
    constructor(props){
        super(props)
        this.state = {
            showOverlay:props.showOverlay
        }

        // this.el = document.createElement('div');
        // this.onHide = this.onHide.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        if(this.props != nextProps) {
            this.setState({
                showOverlay: nextProps.showOverlay
            });
        }
    }

    // onHide(){
    //     const { container } = this.props
    //     let that = this
    //
    //     this.setState({ showOverlay:false },()=>{
    //         ReactDOM.unmountComponentAtNode(container)
    //         container.removeChild(that.el)
    //     })
    // }

    render(){
        const { onClose } = this.props
        const { showOverlay } = this.state

        // let childrenWithProps = React.Children.map(this.props.children, child =>
            // React.cloneElement(child, {
                // onHide:this.onHide
            // }));

        return (
            // showOverlay ?
            // ReactDOM.createPortal(
            //     <div className="overlay-wrapper">
            //         <div className="overlay-table">
            //             <div className="overlay-cell">
            //
            //                 <div className="overlay-placeholder-wrapper">
            //                     <div className="overlay-close" onClick={(e)=>{
            //                         // this.onHide()
            //
            //                         if(onClose)
            //                             onClose(e)
            //                     }}>
            //                         <FontIcon>close</FontIcon>
            //                     </div>
            //                     {this.props.children}
            //                 </div>
            //             </div>
            //         </div>
            //
            //     </div>,
            //     this.el,
            // ): null
            <div className={showOverlay ? 'overlay-wrapper' : 'overlay-wrapper hide'}>
                <div className="overlay-table">
                    <div className="overlay-cell">

                        <div className="overlay-placeholder-wrapper">
                            <div className="overlay-close" onClick={(e)=>{
                                // this.onHide()
                                this.props.toggleOverlay()
                                if(onClose)
                                    onClose(e)
                            }}>
                                <FontIcon>close</FontIcon>
                            </div>
                            {this.props.children}
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Overlay;

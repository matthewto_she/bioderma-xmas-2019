// resources/assets/js/components/App.js

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import Overlay from './Overlay'
import AdventCalendarSliderDesktop from "./AdventCalendarSliderDesktop";
import AdventCalendarSlider from "./AdventCalendarSlider";

class OverlayAdventCalendarSlider extends Component {
    constructor(props){
        super(props)
        this.state = {
            showOverlay: false,
            name: 0
        }

        // this.el = document.createElement('div');
        // this.onHide = this.onHide.bind(this)
        this.toggleOverlay = this.toggleOverlay.bind(this);
        // this.slickGoTo = this.slickGoTo.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    toggleOverlay(name){
        this.setState({
            name: name
        })
        let that = this
        that.setState({
            showOverlay: !that.state.showOverlay
        })
        // setTimeout(function(){
        //     that.setState({
        //         showOverlay: !that.state.showOverlay
        //     }, () => {
        //         // if(that.state.showOverlay){
        //         //     let data = {
        //         //         "campaign-name": 'sanofi',
        //         //         "event-action": 'Click',
        //         //         "action-label":  'interview-video' + (that.state.name+1)
        //         //     };
        //         //     dataLayer.push(Object.assign({
        //         //         event:"behavior-event",
        //         //     }, data));
        //         // }
        //     })
        // }, (that.state.showOverlay) ? 0 : 400);


    }

    // slickGoTo(name){
    //     this.setState({
    //         name: name + 1
    //     })
    // }

    onClose(){
        this.setState({name: 0})
        $('.embed-container').each(function(){
            $('iframe').attr('src', $('iframe').attr('src'));
        });
    }

    render () {

        if(document.getElementById('advent-calendar-slider-desktop')) {
            ReactDOM.render(<AdventCalendarSliderDesktop
                toggleOverlay={this.toggleOverlay}
                // slickGoTo={this.slickGoTo}
            />, document.getElementById('advent-calendar-slider-desktop'))
        }
        return (
            <Overlay
                showOverlay={this.state.showOverlay}
                toggleOverlay={this.toggleOverlay}
                onClose={this.onClose}
            >
                <AdventCalendarSlider slickGoTo={this.state.name + 1}></AdventCalendarSlider>
            </Overlay>
        )

    }
}



export default OverlayAdventCalendarSlider;
let common = {}

if(typeof document != 'undefined'){
    common = {
        baseURL: document.querySelector('meta[name="index_url"]') ? document.querySelector('meta[name="index_url"]').content : '',
        csrf_token:document.querySelector('meta[name="csrf-token"]') ?  document.querySelector('meta[name="csrf-token"]').content : '',
    }
}

module.exports = common
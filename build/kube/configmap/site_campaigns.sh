#!/bin/bash

SITES=_pipline 

php /code/artisan migrate --force

mkdir -p /nfs_share/campaigns.she.com/$SITES/resized

chmod -R 777 /nfs_share/campaigns.she.com/$SITES

ln -s -b /nfs_share/campaigns.she.com/$SITES /code/storage/app/public/upload


#!/bin/bash


aws ecr list-images --repository-name _repository-name_ --query 'imageIds[?type(imageTag)!=`string`].[imageDigest]' --output text | while read line; do aws ecr batch-delete-image --repository-name _repository-name_ --image-ids imageDigest=$line; done	
	
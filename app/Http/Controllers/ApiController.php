<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Carbon\Carbon;

class ApiController extends BaseController
{
    public function adventcalendar(){
        $dateEnd = Carbon::create(2019, 12, 10, 0, 0, 0, 'Asia/Hong_Kong');
        $dateCurrent =  Carbon::now();
        return $dateEnd->diffInDays($dateCurrent, false);
    }

}